const Blockchain = require("./blockchain");

const bitcoin = new Blockchain();

// bitcoin.creatNewBlock(1326505, "afhgha346ntyf", "lkdjf435Agjayoiu5hy");
// bitcoin.createNewTransaction(100, "amir65ghsgh6", "nasrins5hs32gh5");
// bitcoin.creatNewBlock(946565, "lkdjf435Agjayoiu5hy", "bvngcbv,nbv");

// bitcoin.createNewTransaction(50, "amir65ghsgh6", "nasrins5hs32gh5");
// bitcoin.createNewTransaction(300, "amir65ghsgh6", "nasrins5hs32gh5");
// bitcoin.createNewTransaction(2000, "amir65ghsgh6", "nasrins5hs32gh5");

// bitcoin.creatNewBlock(654856, "bvngcbv,nbv", "asddf5gh457gfsg5");

// const currentBlockData = [
//   {
//     amount: 10,
//     sender: "amir65ghsgh6",
//     recipient: "nasrins5hs32gh5",
//   },
//   {
//     amount: 30,
//     sender: "nasrins5hs32gh5",
//     recipient: "amir65ghsgh6",
//   },
//   {
//     amount: 50,
//     sender: "amir65ghsgh6",
//     recipient: "alislkfglkg54",
//   },
// ];

// const nonce = bitcoin.proofOfWork("lkajasdasdasdsdlaskd", currentBlockData);
// const hash = bitcoin.hashBlock("lkajasdasdasdsdlaskd", currentBlockData, nonce);

const bc1 = [
  {
    index: 1,
    timestamp: 1633848354765,
    transactions: [],
    nonce: 100,
    hash: "0",
    previousBlockHash: "0",
  },
  {
    index: 2,
    timestamp: 1633848418382,
    transactions: [
      {
        amount: 50,
        sender: "amir65ghsgh6",
        recipient: "nasrinADSfjk4",
        transactionId: "cefe75c52a5144afae69b98c1bc733e5",
      },
      {
        amount: 40,
        sender: "amir65ghsgh6",
        recipient: "nasrinADSfjk4",
        transactionId: "aa84885fcea64aab82723eca2a077fb2",
      },
      {
        amount: 500,
        sender: "amir65ghsgh6",
        recipient: "nasrinADSfjk4",
        transactionId: "4dcaa94c000a46ef92f0cfb2f70b65ee",
      },
    ],
    nonce: 122080,
    hash: "00004ca74ae210ac886fa1e39e1ba9a8bf92bc07db57c53cc58ef5b38fbb3c73",
    previousBlockHash: "0",
  },
  {
    index: 3,
    timestamp: 1633848432097,
    transactions: [
      {
        amount: 12.5,
        sender: "00",
        recipient: "573c8fd92dc94236bc832ff93c071feb",
        transactionId: "8fdc005bd3bf4536b343e45ee0970438",
      },
    ],
    nonce: 78413,
    hash: "0000c56b322933161b3e0c34d9028c16c5c748e86e0fa806cbec3a2f4f3a9c08",
    previousBlockHash:
      "00004ca74ae210ac886fa1e39e1ba9a8bf92bc07db57c53cc58ef5b38fbb3c73",
  },
];

const chainIsValid = bitcoin.chainIsValid(bc1);

console.log("isValid: ", chainIsValid);
